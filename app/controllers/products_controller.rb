class ProductsController < ApplicationController
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product.category, notice: 'El producto ha sido creado.' }
      else
        format.html { render :new}
      end
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to @product.category, notice: 'El producto fue eliminado' 
  end

  private
  def product_params
    params.require(:product).permit(:name, :price, :category_id)
  end
end
